<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
<s:head/>
<head>
<title>Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles/w3.css">
<link rel="stylesheet" href="styles/w3-colors-windows.css">
<link rel="stylesheet" href="styles/font-awesome-4.7.0/css/font-awesome.min.css">
</head>
<body onload="unBlockButton('submit');" class="w3-content w3-dark-grey" style="max-width:450px"> 	 
    <s:form action="login" cssClass="w3-container w3-card-4 w3-light-grey w3-margin" onsubmit="blockButton('submit');">
    <s:token/>
    <h3 class="w3-xlarge w3-center">Inicio de Sesi�n</h3>
     <s:if test="message!=null">
		<div class="w3-panel w3-pale-red w3-display-container">
		    <p class="w3-text-deep-orange"><s:property value="message"/></p>
		    <s:fielderror cssClass="w3-text-red w3-small" fieldName="message"/>
		</div> 
	</s:if>
	<div class="w3-row w3-section">	
		<div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"aria-hidden="true"></i></div>
		<div class="w3-rest">
			<s:fielderror cssClass="w3-text-red w3-small" fieldName="name"/>
			<s:textfield cssClass="w3-input w3-border" name="name" placeholder="Usuario" required="true" maxlenght="50"/>
		</div>
	</div>
	<div class="w3-row w3-section">
		<div class="w3-col" style="width:50px"><i class="w3-xlarge fa fa-key" aria-hidden="true"></i></div>
		<div class="w3-rest">
			<s:fielderror cssClass="w3-text-red w3-small" fieldName="password"/>
			<s:password name="password" cssClass="w3-input w3-border" placeholder="Password" required="true" maxlenght="50"/>
		</div>
	</div>
	
     <s:submit cssClass="w3-button w3-block w3-section w3-blue-grey w3-ripple w3-padding" id="submit" value="Login" />
     
    </s:form>
    
</body>
</html>

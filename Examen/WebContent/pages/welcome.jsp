<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
<s:head/>
<head>
<title>Bienvenido</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles/w3.css">
<link rel="stylesheet" href="styles/w3-colors-windows.css">
<link rel="stylesheet" href="styles/font-awesome-4.7.0/css/font-awesome.min.css">
<style>
body {margin:0;}

.icon-bar {
  width: 100%;
  background-color: #555;
  overflow: auto;  
}

.icon-bar a {
  float: left;
  width: 20%;
  text-align: center;
  padding: 2px 0;
  transition: all 0.3s ease;
  color: white;
  font-size: 30px;
}

.icon-bar a:hover {
  background-color: #000;
}

.active {
  background-color: #04AA6D;
}
</style>
</head>
<body onload="unBlockButton('submit');" class="w3-content w3-light-grey" style="max-width:450px">
    <h3 class="w3-xxlarge w3-center">BIENVENIDO</h3>
    <div class="icon-bar">
	  <a class="active" href="#"><i class="fa fa-home"></i></a> 
	  <a href="#"><i class="fa fa-search"></i></a> 
	  <a href="#"><i class="fa fa-envelope"></i></a> 
	  <a href="#"><i class="fa fa-globe"></i></a>
	  <a href="#"><i class="fa fa-trash"></i></a> 
</div>
</body>
</html>
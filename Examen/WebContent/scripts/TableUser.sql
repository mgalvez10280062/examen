CREATE TABLE usuario (
  login int NOT NULL AUTO_INCREMENT
 ,password varchar(30) NOT NULL
 ,nombre varchar(50) NOT NULL
 ,cliente float NOT NULL
 ,email varchar(50) DEFAULT NULL
 ,fechaalta date NOT NULL
 ,fechabaja date DEFAULT NULL
 ,status char(1) NOT NULL
 ,intentos float NOT NULL
 ,fecharevocado date DEFAULT NULL
 ,fecha_vigencia date DEFAULT NULL
 ,no_acceso int DEFAULT NULL
 ,apellido_paterno varchar(50) DEFAULT NULL
 ,apellido_materno varchar(50) DEFAULT NULL
 ,area decimal(10,0) DEFAULT NULL
 ,fechamodificacion date NOT NULL
 ,PRIMARY KEY (login)
);
package com.examen.dao;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.examen.business.Usuario;
import com.examen.utils.SQLExecution;
import com.examen.utils.SQLResult;
import com.examen.utils.SQLSelectResult;

public class UsuarioDao {

	private Usuario usuario;
	private SQLExecution exe;
	
	public UsuarioDao()
	{
		exe = new SQLExecution();
	}
	
	public UsuarioDao(SQLExecution exe)
	{
		this.exe = exe;	
	}
	
//	Administración de transacciones
	public void setAutoCommit(boolean autoCommit) {
		exe.setAutoCommit(autoCommit);
	}
	
	public boolean commit()
	{
		return exe.commit();
	}
	
	public boolean rollBack()
	{
		return exe.rollBack();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	/** 
	 * Regresa el password asignado al usuario.
	 * @return continue=Todo salio bien, error=Si hubo un problema
	 */
	public SQLResult getPassword()
	{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";
		String fechaActual = "";

		String SQL = "SELECT login, password, CAST(fechamodificacion AS char(11)) as fechamodificacion, CAST(CURDATE() AS char(11)) as fechaactual " + 
				 "FROM USUARIO " + 
				 "WHERE nombre=? ";
		
		params.add(usuario.getUsuario());
		
		SQLSelectResult result = exe.executeSelect(SQL, params);
		
		if(result.getMessage().equals("continue"))
		{			
			List<Hashtable<String,Object>> lista = result.getListResult();
			if(lista.size()>0)
			{
				Hashtable<String, Object> ht = lista.get(0);
				usuario.setIdUsuario((int) ht.get("login"));
				usuario.setPassword((String) ht.get("password"));	
				usuario.setFechaModificacion((String) ht.get("fechamodificacion"));
				fechaActual = (String) ht.get("fechaactual");					
				if(ht.get("fechaactual").equals(usuario.getFechaModificacion()))
				{
					estatus = "caducado";
				}
			}
			else
			{
				sResult.setDetalle("Usuario incorrecto intente nuevamente.");
				estatus = "input";
			}
		}
		else
		{
			sResult.setDetalle("Error:(US0004) Ocurrió un error al consultar al usuario, favor de verificar.");
			if(result.getEx()!=null)System.err.println("Error:(US0004)"+result.getEx().getMessage());
			estatus = "error";
		}
		
		sResult.setEstatus(estatus);
		return sResult;
	}
	
	public SQLResult updatePassword()
	{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";

		String SQL = "UPDATE USUARIO "+
					"SET password = ? "+
					",fechamodificacion = DATE(DATE_ADD(now(), INTERVAL 1 MONTH)) " +					
					"WHERE login = ? ";

		params.add(usuario.getPassword());		
		params.add(usuario.getIdUsuario());
		
		SQLSelectResult result = exe.executeUpdate(SQL, params);
		int affectedRows = result.getAffectedRows();
				
		if (affectedRows == 0) {
			sResult.setDetalle("Error:(US0009) No se pudo actualizar fecha de modificacion, favor de verificar.");
			if(result.getEx()!=null)System.err.println("Error:(US0009)"+result.getEx().getMessage());
			estatus = "error";
		}
		
		sResult.setEstatus(estatus);
		return sResult;
	}
	
}

package com.examen.action;


import java.util.Base64;
//import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.examen.business.Usuario;
import com.examen.dao.UsuarioDao;
import com.examen.utils.SQLResult;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport implements SessionAware {

	private static final long serialVersionUID = 1L;
	private Map<String, Object> userSession;
	
	private String name;
	private String password;
	private String message;
	private Usuario usuario;
	
	//M�todo para utilizar la sesi�n del usuario
	public void setSession(Map<String, Object> session) {
	   userSession = session ;
	}  
	
	public String getName() {
		return name;
	}
 
	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	 
	public String execute() {
		SQLResult sResult = null;
		UsuarioDao usrDao = new UsuarioDao();
		String estatus = "";
		
		usuario = new Usuario();
		usuario.setUsuario(this.getName());
		usrDao.setUsuario(usuario);
		sResult = usrDao.getPassword();
		estatus = sResult.getEstatus();
		message = sResult.getDetalle();	 
		
		if(estatus.equals("continue"))
		{
			byte[] decodedBytes = Base64.getDecoder().decode(usuario.getPassword());
			String decodedString = new String(decodedBytes);	    
			if(decodedString.equals(this.getPassword()))
			{			
				estatus = "continue";
				return estatus;
			}
			else
			{
				message = "Password incorrecto intente nuevamente";
				return "input";
			}
		}
		System.out.println("Estatus: " + estatus);
		return estatus;
	}

	public String updatePassword()
	{
		SQLResult sResult = null;
		UsuarioDao usrDao = new UsuarioDao();
		String estatus = "";
		
		String encodedPass = Base64.getEncoder().encodeToString(this.password.getBytes());			
		usuario.setPassword(encodedPass);		
		usrDao.setUsuario(usuario);
		sResult = usrDao.updatePassword();
		estatus = sResult.getEstatus();
		
		return estatus;
	}

}

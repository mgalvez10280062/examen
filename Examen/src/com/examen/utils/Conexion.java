package com.examen.utils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class Conexion 
{
	
//	private int noCon;
//	private String estatus = null;
//	private Connection con = null;
////private String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=Mondelez;integratedSecurity=true;"; 
////<<<<<<< HEAD
////	private String connectionUrl = "jdbc:sqlserver://10.50.180.208;" + "databaseName=banorteTdc_dev;" + "user=BanorteTarjetasUsr;" + "password=T4rj3tas_1;";
////	private String connectionUrl = "jdbc:sqlserver://10.50.180.208;" + "databaseName=banorteTdc_qa;" + "user=BanorteTarjetasUsr;" + "password=T4rj3tas_1;";
////	private String connectionUrl = "jdbc:sqlserver://IMMTOLD008514\\SQLEXPRESS;" + "databaseName=banorteTdc_dev;" + "user=BanorteTarjetasUsr;" + "password=T4rj3tas_1;";
////	private String connectionUrl = "jdbc:sqlserver://localhost:1433;" + "databaseName=banorteTdc_dev;" + "user=SA;" + "password=123456;";
////=======
////	private String connectionUrl = "jdbc:sqlserver://10.50.180.208;" + "databaseName=banorteTdc_dev;" + "user=BanorteTarjetasUsr;" + "password=T4rj3tas_1;";
////	private String connectionUrl = "jdbc:sqlserver://10.50.180.208;" + "databaseName=banorteTdc_qa;" + "user=BanorteTarjetasUsr;" + "password=T4rj3tas_1;";
////	private String connectionUrl = "jdbc:sqlserver://IMMTOLD008514\\SQLEXPRESS;" + "databaseName=banorteTdc_dev;" + "user=BanorteTarjetasUsr;" + "password=T4rj3tas_1;";	
//	private String connectionUrl = "jdbc:sqlserver://localhost:1433;" + "databaseName=banorteTdc_dev;" + "user=SA;" + "password=123456;";
////>>>>>>> 777e007ac74169b3a9cbd3bfb1b151e67d090953
////private String connectionUrl = "jdbc:sqlserver://10.50.180.145\\KOFAX;" + "databaseName=bpm_facturas;" + "user=Kofaxusr;" + "password=Kofax!1;";
////private String connectionUrl = "jdbc:sqlserver://MXPESSSQLP001\\pesqueria01;" + "databaseName=banorte_fdv;" + "user=Kofaxusr;" + "password=Kofax!1;";
//	
//	public int getNoCon() {
//		return noCon;
//	}
//	public void setNoCon(int noCon) {
//		this.noCon = noCon;
//	}
//	public String getEstatus() {
//		return estatus;
//	}
//	public void setEstatus(String estatus) {
//		this.estatus = estatus;
//	}
//	public Connection getCon(boolean autoCommit) {
//		setAutoCommit(autoCommit);
//		return con;
//	}
//	public Connection getCon() {
//		return con;
//	}
//	public void setCon(Connection con) {
//		this.con = con;
//	}
//
//	public void establishConnection_old()
//	{
//		if(con == null)
//		{
//	  		try 
//	  		{
//	  			// Establish the connection.
//				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//	      		con = DriverManager.getConnection(connectionUrl);
//	  		} catch (ClassNotFoundException e) {
//				e.printStackTrace();
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		}
//	}
//	
//	public void setAutoCommit(boolean type){
//		try {
//			con.setAutoCommit(type);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	public void commit()
//	{
//		try {
//			con.commit();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}	
//	
//	public void closeConnection()
//	{
//		
//		if (con != null) 
//		{
//			try { 
//				con.close(); 
//				con = null;
//			} 
//			catch(Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}
//	
//	public void establishConnection()
//	{
//		
//		try
//		{
//			InitialContext initContext = new InitialContext();
//		    DataSource ds = (DataSource) initContext.lookup("java:/comp/env/jdbc/ConexionSQLServer");
//		    con = ds.getConnection();
//		}
//		catch (SQLException e) 
//		{
//			e.printStackTrace();
//		}
//		catch (NamingException e) 
//		{
//			e.printStackTrace();
//		}
//		
//	}
//}
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
	
	private String bd = "develop";
	private String user = "marco";
	private String pwd = "123456";
	private String connectionUrl = "jdbc:mysql://localhost/" + bd;

	private int noCon;
	private String estatus = null;
	private Connection con = null;
	
	public int getNoCon() {
		return noCon;
	}
	public void setNoCon(int noCon) {
		this.noCon = noCon;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public Connection getCon(boolean autoCommit) {
		setAutoCommit(autoCommit);
		return con;
	}
	public Connection getCon() {
		return con;
	}
	public void setCon(Connection con) {
		this.con = con;
	}

	public void establishConnection(){
		
		if(con == null)
		{
	  		try {
	  			// Establish the connection.
	  			Class.forName("com.mysql.jdbc.Driver");
	      		con = DriverManager.getConnection(connectionUrl, user, pwd);	      		
	  		} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setAutoCommit(boolean type){
		try {
			con.setAutoCommit(type);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void commit()
	{
		try {
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}	
	
	public void closeConnection()
	{
		
		if (con != null) 
		{
			try { 
				con.close(); 
				con = null;
			} 
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void establishConnection_new(){
		
		try {
			InitialContext initContext = new InitialContext();
		    DataSource ds = (DataSource) initContext.lookup("java:/comp/env/jdbc/ConexionSQLServer");
		    con = ds.getConnection();
		}catch (SQLException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
	}	
}

package com.examen.utils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Hashtable;
import java.util.List;

public class SQLExecution {

	private SQLServerConnection instance = null;
	private Connection con;
	private boolean autoCommit = true;
	
	public SQLExecution()
	{
		instance = SQLServerConnection.getInstance();
	}
	
	public Connection getCon() {
		if(con==null)
			con = instance.getConnection().getCon();
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}
	
	public boolean isAutoCommit() {
		return autoCommit;
	}

	public void setAutoCommit(boolean autoCommit) {
		this.autoCommit = autoCommit;
		if(con == null)
			con = instance.getConnection().getCon();
		try{			
			con.setAutoCommit(autoCommit);
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public boolean commit()
	{
		boolean result = true;
		if(con != null)
		{
			try{
				con.commit();
				con.close();
				con = null;
			}
			catch(SQLException e)
			{
				e.printStackTrace();
				result = false;
			}
		}
		
		return result;
	}
	
	public boolean rollBack()
	{
		boolean result = true;
		if(con != null)
		{
			try{				
				con.rollback();
				con.close();
				con = null;
			}
			catch(SQLException e)
			{
				e.printStackTrace();
				result = false;
			}
		}
		return result;
	}
	
	public void closeConnection()
	{
		if (con != null) 
		{
			try { 
				con.close(); 
				con = null;
			} 
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	
	public SQLSelectResult executeSelect(String SQL, List<Object> params) {
		SQLSelectResult result = new SQLSelectResult();  
		ResultSet rs = null;
		Hashtable<String,Object> ht = null;
		
		this.getCon();
		
			try (PreparedStatement stmt = con.prepareStatement(SQL))
			{
				placeParams(params, stmt);
		  		rs = stmt.executeQuery();
		  		
		  		ResultSetMetaData  Mdata = rs.getMetaData();
		  		
	  			while (rs.next()) 
	  			{
	  				
	  				ht = new Hashtable<String,Object>();
	  				for(int i=1; i<=Mdata.getColumnCount();i++)
			  		{
	  					
	  					if(rs.getObject(i) == null)
	  					{
	  						
	  						int tp = Mdata.getColumnType(i);
	  						if(Types.VARCHAR == tp || Types.CHAR == tp || Types.LONGNVARCHAR == tp || Types.LONGVARCHAR == tp)
	  							ht.put(Mdata.getColumnName(i),"");
	  						else if(Types.TIME == tp|| Types.TIME_WITH_TIMEZONE == tp)
	  							ht.put(Mdata.getColumnName(i),new Time(0L));
	  						else if(Types.DATE == tp)
	  							ht.put(Mdata.getColumnName(i),new Date(0L));
	  						else if(Types.TIMESTAMP == tp  || Types.TIMESTAMP_WITH_TIMEZONE == tp)
	  							ht.put(Mdata.getColumnName(i),new Timestamp(0L));
	  						else
	  							ht.put(Mdata.getColumnName(i),0);
	  					}
	  					else
	  					{
	  						ht.put(Mdata.getColumnLabel(i), rs.getObject(i));
	  					}
			  		}
		  			result.getListResult().add(ht);
		  		}
		  		
		  		result.setMessage("continue");
		  		if(this.isAutoCommit())
		  			this.closeConnection();
			} catch (Exception e) 
			{
				result.setMessage("error");
				result.setEx(e); 
				if(!this.isAutoCommit())
					this.rollBack();
				this.closeConnection();
				return result;
			}
			
			return result;
	}
	
	public SQLSelectResult executeUpdate(String SQL, List<Object> params) {
		
		SQLSelectResult result = new SQLSelectResult();
		this.getCon();
		
		try (PreparedStatement stmt = con.prepareStatement(SQL)) {
			placeParams(params, stmt);
			
		int affectedRows = stmt.executeUpdate();
		result.setAffectedRows(affectedRows);
        if (affectedRows == 0) {
            throw new SQLException("UPDATE failed, no rows affected.");
        }
        result.setMessage("continue");
        if(this.isAutoCommit())
  			this.closeConnection();
	} catch (SQLException e) {					
		result.setMessage("error");
		result.setEx(e);
		if(!this.isAutoCommit())
			this.rollBack();
		this.closeConnection();
//		e.printStackTrace();
		return result;
	}
	
	return result;
	
	}
		
	private void placeParams(List<Object> params, PreparedStatement stmt) throws SQLException {
		for(int i=1;i<=params.size();i++)
		{
			Object data =  params.get(i-1);
			if(data instanceof String)
				stmt.setString(i, (String) data);
			else if(data instanceof Integer)
				stmt.setInt(i,((Integer)data).intValue());
			else if(data instanceof Date)
				stmt.setDate(i, (Date)data);
			else if(data instanceof Long)
				stmt.setLong(i, ((Long)data).longValue());
			else if(data instanceof Timestamp)
				stmt.setTimestamp(i, (Timestamp) data);
			else if(data instanceof Double)
				stmt.setDouble(i, (Double) data);
			else if(data instanceof Float)
				stmt.setFloat(i, (Float) data);
			else if(data==null)
				stmt.setString(i, "");
			else
				stmt.setString(i, "DEFAULT:"+data.toString());
		}
	}
}

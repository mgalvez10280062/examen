package com.examen.utils;

import java.util.ArrayList;
import java.util.Hashtable;

public class SQLServerConnection {

	private static int poolsize = 2;
	private static int timer = 1000;
	
	private Hashtable<String,Conexion> conexiones = null;
	private ArrayList<String> conexionesLibres  = null;
	private ArrayList<String> conexionesUsadas = null;
	
	private static SQLServerConnection instance = null;
	private static SQLServerConnection pl_instance = null;
	
	private SQLServerConnection()
	{
		
	}
	
	private SQLServerConnection(int param)
	{
		conexiones = new Hashtable<String,Conexion>();
		conexionesLibres = new ArrayList<String>();
		conexionesUsadas = new ArrayList<String>();
		for(int i=0; i<poolsize; i++)
		{
			Conexion conn = new Conexion();
			conn.establishConnection();
			conn.setNoCon(i);
			conn.setEstatus("libre");
			conexiones.put(String.valueOf(i),conn);
			conexionesLibres.add(String.valueOf(i));
		}
	}
	
	public static SQLServerConnection getInstance()
	{
		if(instance == null)
		{
			instance = new SQLServerConnection();
		}
		
		return instance;
	}
	
	public static SQLServerConnection getInstance(int param)
	{
		if(pl_instance == null)
		{
			pl_instance = new SQLServerConnection(param);
		}
		
		return pl_instance;
	}
	
	public Conexion getConnection(){
		Conexion conn = new Conexion();
		conn.establishConnection();
		return conn;
	}
	
}

package com.examen.utils;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class SQLSelectResult {

	private String message;
	private Exception ex;
	private List<Hashtable<String,Object>> listResult;
	private int affectedRows;
	private int key;
	
	public SQLSelectResult(){
		this.listResult = new ArrayList<Hashtable<String,Object>>();
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Exception getEx() {
		return ex;
	}
	public void setEx(Exception ex) {
		this.ex = ex;
	}

	public List<Hashtable<String, Object>> getListResult() {
		return listResult;
	}

	public void setListResult(List<Hashtable<String, Object>> listResult) {
		this.listResult = listResult;
	}

	public int getAffectedRows() {
		return affectedRows;
	}

	public void setAffectedRows(int affectedRows) {
		this.affectedRows = affectedRows;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}
	
}

package com.im.dao;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.im.business.Rol;
import com.im.business.Usuario;
import com.im.catalogos.Area;
import com.im.catalogos.Dir;
import com.im.catalogos.Grupo;
import com.im.catalogos.Vice;
import com.im.utils.SQLExecution;
import com.im.utils.SQLResult;
import com.im.utils.SQLSelectResult;

public class UsuarioDao {

	private Usuario usuario;
	private SQLExecution exe;
	
	public UsuarioDao()
	{
		exe = new SQLExecution();
	}
	
	public UsuarioDao(SQLExecution exe)
	{
		this.exe = exe;	
	}
	
//	Administración de transacciones
	public void setAutoCommit(boolean autoCommit) {
		exe.setAutoCommit(autoCommit);
	}
	
	public boolean commit()
	{
		return exe.commit();
	}
	
	public boolean rollBack()
	{
		return exe.rollBack();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/** 
	 * Carga la lista de usuarios existentes con base a un Rol.
	 * @return continue=Todo salio bien, error=Si hubo un problema
	 */
	public SQLResult loadUsuarios(int idRol)
	{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";

		String SQL ="SELECT      TW_Usuarios.idUsuario, TW_Usuarios.usuario, TW_Usuarios.nombre, TW_Usuarios.email, TW_Usuarios.rfc, "+
				    			"TC_RolGrupo.idRol, TC_Rol.descripcion AS Rol "+
					"FROM        TW_Usuarios INNER JOIN "+
								"TC_Grupo ON TW_Usuarios.idGrupo = TC_Grupo.idGrupo INNER JOIN "+
								"TC_RolGrupo ON TC_Grupo.idGrupo = TC_RolGrupo.idGrupo INNER JOIN "+
								"TC_Rol ON TC_RolGrupo.idRol = TC_Rol.idRol "+
					"WHERE       TC_RolGrupo.idRol = ? "+ 
					"ORDER BY    TW_Usuarios.nombre ";
		
		params.add(idRol);
		
		SQLSelectResult result = exe.executeSelect(SQL, params);
		
		if(result.getMessage().equals("continue"))
		{
			List<Hashtable<String,Object>> lista = result.getListResult();
			List<Usuario> usuarios = new ArrayList<Usuario>();
			if(lista.size()>0)
			{
				for(int i=0; i<lista.size();i++)
				{
					Hashtable<String,Object> ht = lista.get(i);
					Usuario us = new Usuario((int)ht.get("idUsuario"),(String)ht.get("nombre"));
					us.setUsuario((String)ht.get("usuario"));
					us.setEmail((String)ht.get("email"));
					us.setRfc((String)ht.get("rfc"));
						Rol rol = new Rol();
						rol.setIdRol((int)ht.get("idRol"));
						rol.setDecripcion((String)ht.get("Rol"));
					us.setRol(rol);
					usuarios.add(us);
				}
				sResult.setObject(usuarios);
			}
			else
			{
				sResult.setDetalle("Error:(US0001) No se localizaron usuarios con los criterios asignados, favor de verificar.");
				estatus = "error";
			}
		}
		else
		{
			sResult.setDetalle("Error:(US0002) No se pudieron consultar los usuarios, favor de verificar.");
			if(result.getEx()!=null)System.err.println("Error:(US0002)"+result.getEx().getMessage());
			estatus = "error";
		}
		
		sResult.setEstatus(estatus);
		return sResult;
	}
	
	/** 
	 * Regresa el password asignado al usuario.
	 * @return continue=Todo salio bien, error=Si hubo un problema
	 */
	public SQLResult getPassword()
	{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";

		String SQL = "SELECT idUsuario, password " + 
				 "FROM TW_Usuarios " + 
				 "WHERE usuario=?";
		
		params.add(usuario.getUsuario());
		
		SQLSelectResult result = exe.executeSelect(SQL, params);
		
		if(result.getMessage().equals("continue"))
		{
			List<Hashtable<String,Object>> lista = result.getListResult();
			if(lista.size()>0)
			{
				Hashtable<String, Object> ht = lista.get(0);
				usuario.setIdUsuario((int) ht.get("idUsuario"));
				usuario.setPassword((String) ht.get("password"));					
			}
			else
			{
				sResult.setDetalle("Datos incorrectos intente nuevamente.");
				estatus = "input";
			}
		}
		else
		{
			sResult.setDetalle("Error:(US0004) Ocurrió un error al consultar al usuario, favor de verificar.");
			if(result.getEx()!=null)System.err.println("Error:(US0004)"+result.getEx().getMessage());
			estatus = "error";
		}
		
		sResult.setEstatus(estatus);
		return sResult;
	}

	/** 
	 * Carga la información del usuario.
	 * @return continue=Todo salio bien, error=Si hubo un problema
	 */
	public SQLResult loadDataUsuario()
	{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";

		String SQL ="SELECT       TW_Usuarios.nombre, TW_Usuarios.email, TW_Usuarios.rfc, TW_Usuarios.idGrupo, TC_Grupo.descripcion AS Grupo, "+
				 				 "TW_Usuarios.idArea, TC_Area.descripcion AS Area, TC_Area.gerente, TC_Area.idDir, TC_Direccion.descripcion AS Dir,  TC_Direccion.idVice, TC_Vicepr.descripcion AS Vice "+
					"FROM         TW_Usuarios INNER JOIN "+
		                         "TC_Grupo ON TW_Usuarios.idGrupo = TC_Grupo.idGrupo INNER JOIN "+
		                         "TC_Area ON TW_Usuarios.idArea = TC_Area.idArea INNER JOIN "+
		                         "TC_Direccion ON TC_Area.idDir = TC_Direccion.idDireccion INNER JOIN "+
		                         "TC_Vicepr ON TC_Direccion.idVice = TC_Vicepr.idVice "+
					"WHERE        TW_Usuarios.idUsuario = ?";
		
		params.add(usuario.getIdUsuario());
		
		SQLSelectResult result = exe.executeSelect(SQL, params);
		
		if(result.getMessage().equals("continue"))
		{
			List<Hashtable<String,Object>> lista = result.getListResult();
			if(lista.size()>0)
			{
				Hashtable<String,Object> ht = lista.get(0);
				usuario.setNombre((String)ht.get("nombre"));
				usuario.setEmail((String)ht.get("email"));
				usuario.setRfc((String)ht.get("rfc"));
					Grupo grupo = new Grupo();
					grupo.setId((int)ht.get("idGrupo"));
					grupo.setDescripcion((String)ht.get("Grupo"));
				usuario.setGrupo(grupo);
					Area area = new Area();
					area.setId((int)ht.get("idArea"));
					area.setDescripcion((String)ht.get("Area"));
					area.setGerente((String)ht.get("gerente"));
					area.setIdDir((int)ht.get("idDir"));
				usuario.setArea(area);
					Dir dir = new Dir();
					dir.setIdDireccion((int)ht.get("idDir"));
					dir.setDescripcion((String)ht.get("Dir"));
					dir.setIdVice((int)ht.get("idVice"));
				usuario.setDir(dir);					
					Vice vice = new Vice();
					vice.setIdVice((int)ht.get("idVice"));
					vice.setDescripcion((String)ht.get("Vice")); 
				usuario.setVice(vice);
				
			}
			else
			{
				sResult.setDetalle("Error:(US0005) No se localizaron usuarios con los criterios asignados, favor de verificar.");
				if(result.getEx()!=null)System.err.println("Error:(US0005)"+result.getEx().getMessage());
				estatus = "error";
			}
		}
		else
		{
			sResult.setDetalle("Error:(US0006) No se pudieron consultar los datos del usuario, favor de verificar.");
			if(result.getEx()!=null)System.err.println("Error:(US0006)"+result.getEx().getMessage());
			estatus = "error";
		}
		
		sResult.setEstatus(estatus);
		return sResult;
	}
	
	
	/** 
	 * Carga los roles asignados al grupo del usuario.
	 * @return continue=Todo salio bien, error=Si hubo un problema
	 */
	public SQLResult getRolesUsuario()
	{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";

		String SQL ="SELECT       TC_RolGrupo.idRol, TC_Rol.descripcion AS Rol "+
					"FROM         TW_Usuarios INNER JOIN "+
		                         "TC_RolGrupo ON TW_Usuarios.idGrupo = TC_RolGrupo.idGrupo INNER JOIN "+
		                         "TC_Rol ON TC_RolGrupo.idRol = TC_Rol.idRol "+
					"WHERE        TW_Usuarios.idUsuario = ?";
		
		params.add(usuario.getIdUsuario());
		
		SQLSelectResult result = exe.executeSelect(SQL, params);
		
		if(result.getMessage().equals("continue"))
		{
			List<Hashtable<String,Object>> lista = result.getListResult();
			Hashtable<String,Rol> roles = new Hashtable<String,Rol>();
			if(lista.size()>0)
			{
				for(int i=0; i<lista.size();i++)
				{
					Hashtable<String,Object> ht = lista.get(i);
					Rol rol = new Rol();
					rol.setIdRol((int)ht.get("idRol"));
					rol.setDecripcion((String)ht.get("Rol"));
					roles.put(String.valueOf(rol.getIdRol()), rol);
				}
				usuario.getGrupo().setRoles(roles); 
			}
			else
			{
				sResult.setDetalle("Error:(US0007) No existen roles asignados al usuario, favor de verificar.");
				if(result.getEx()!=null)System.err.println("Error:(US0007)"+result.getEx().getMessage());
				estatus = "error";
			}
		}
		else
		{
			sResult.setDetalle("Error:(US0008) No se pudieron consultar los roles del usuario, favor de verificar.");
			if(result.getEx()!=null)System.err.println("Error:(US0008)"+result.getEx().getMessage());
			estatus = "error";
		}
		
		sResult.setEstatus(estatus);
		return sResult;
	}
	
	
	public SQLResult updateDataUsuario()
	{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";

		String SQL = "UPDATE TW_Usuarios "+
					"SET nombre=?, "+
					"email = ?, "+
					"rfc = ? "+
					"WHERE idUsuario = ? ";

		params.add(usuario.getNombre());
		params.add(usuario.getEmail());
		params.add(usuario.getRfc());
		params.add(usuario.getIdUsuario());
		
		SQLSelectResult result = exe.executeUpdate(SQL, params);
		int affectedRows = result.getAffectedRows();
		
		estatus = "continue";
		if (affectedRows == 0) {
			sResult.setDetalle("Error:(US0009) No se pudo actualizar el usuario, favor de verificar.");
			if(result.getEx()!=null)System.err.println("Error:(US0009)"+result.getEx().getMessage());
			estatus = "error";
		}
		
		sResult.setEstatus(estatus);
		return sResult;
	}
	
	/** 
	 * Carga la lista de usuarios existentes con base a un Rol.
	 * @return continue=Todo salio bien, error=Si hubo un problema
	 */
	public SQLResult loadUsuarios(String roles)
	{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";

		String SQL ="SELECT      TW_Usuarios.idUsuario, TW_Usuarios.usuario, TW_Usuarios.nombre, TW_Usuarios.email, TW_Usuarios.rfc, "+
				    			"TC_RolGrupo.idRol, TC_Rol.descripcion AS Rol "+
					"FROM        TW_Usuarios INNER JOIN "+
								"TC_Grupo ON TW_Usuarios.idGrupo = TC_Grupo.idGrupo INNER JOIN "+
								"TC_RolGrupo ON TC_Grupo.idGrupo = TC_RolGrupo.idGrupo INNER JOIN "+
								"TC_Rol ON TC_RolGrupo.idRol = TC_Rol.idRol "+
					"WHERE       TC_RolGrupo.idRol in ( "+roles+ ")"+ 
					"ORDER BY    TW_Usuarios.nombre ";
		
		
		SQLSelectResult result = exe.executeSelect(SQL, params);
		
		if(result.getMessage().equals("continue"))
		{
			List<Hashtable<String,Object>> lista = result.getListResult();
			List<Usuario> usuarios = new ArrayList<Usuario>();
			if(lista.size()>0)
			{
				for(int i=0; i<lista.size();i++)
				{
					Hashtable<String,Object> ht = lista.get(i);
					Usuario us = new Usuario((int)ht.get("idUsuario"),(String)ht.get("nombre"));
					us.setUsuario((String)ht.get("usuario"));
					us.setEmail((String)ht.get("email"));
					us.setRfc((String)ht.get("rfc"));
						Rol rol = new Rol();
						rol.setIdRol((int)ht.get("idRol"));
						rol.setDecripcion((String)ht.get("Rol"));
					usuarios.add(us);
				}
				sResult.setObject(usuarios);
			}
			else
			{
				sResult.setDetalle("Error:(US0010) No se localizaron usuarios con los criterios asignados, favor de verificar.");
				estatus = "error";
			}
		}
		else
		{
			sResult.setDetalle("Error:(US0011) No se pudieron consultar los usuarios, favor de verificar.");
			if(result.getEx()!=null)System.err.println("Error:(US0011)"+result.getEx().getMessage());
			estatus = "error";
		}
		
		sResult.setEstatus(estatus);
		return sResult;
	}
	
	public SQLResult getUsuarios()
	{
		System.out.println("entra getUsuarios");
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<>();
		String estatus = "continue";
		
		String SQL = "SELECT usuario, password FROM tw_usuarios";
		
		
		SQLSelectResult result = exe.executeSelect(SQL, params);
		
		if(result.getMessage().equals("continue"))
		{
			
			List<Hashtable<String,Object>> lista = result.getListResult();
			List<Usuario> usuarios = new ArrayList<Usuario>();
			
			for (int i = 0; i < lista.size(); i++) 
			{
				Hashtable<String,Object> ht = lista.get(i);
				Usuario usuario = new Usuario();
				usuario.setUsuario((String) ht.get("usuario"));
				usuario.setPassword((String) ht.get("password"));
				usuarios.add(usuario);
			}
			usuarios.forEach(System.out::println);
			sResult.setObject(usuarios);
			estatus = "continue";
		}
		else
		{
			estatus = "error";
			sResult.setEstatus(estatus);
//			throw new QueryException("ERR (BOX0013): No se pudo consultar la información de la Factura:"+factura.getNumDocumento()+", favor de verificar. Err:"+result.getEx().toString());
		}
		sResult.setEstatus(estatus);
		return sResult;	
	
	}
	
}

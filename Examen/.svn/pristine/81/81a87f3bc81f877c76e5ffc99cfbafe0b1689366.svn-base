package com.im.dao;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.im.business.Distribucion;
import com.im.business.Usuario;
import com.im.utils.QueryException;
import com.im.utils.SQLExecution;
import com.im.utils.SQLResult;
import com.im.utils.SQLSelectResult;
import com.im.utils.Utils;

public class DistribucionDao {
	
	private Distribucion distribucion;
	private Usuario usuario;
	private SQLExecution exe;
	
	public Distribucion getDistribucion() {
		return distribucion;
	}
	public void setDistribucion(Distribucion distribucion) {
		this.distribucion = distribucion;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public SQLExecution getExe() {
		return exe;
	}
	public void setExe(SQLExecution exe) {
		this.exe = exe;
	}
	
	public DistribucionDao()
	{
		exe = new SQLExecution();
	}
	
	public DistribucionDao(SQLExecution exe)
	{
		this.exe = exe;	
	}
		
	public void setAutoCommit(boolean autoCommit) {
		exe.setAutoCommit(autoCommit);
	}
	
	public boolean commit()
	{
		return exe.commit();
	}
		
	public boolean rollBack()
	{
		return exe.rollBack();
	}
	
	public SQLResult nuevaDistribucion() throws QueryException{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "error";
		String SQL = "INSERT INTO TW_Distribuciones (fecha,cdc,plaza,ruta,parada,no_tienda,tda,status,idUsuario,paquete)"+
				 "VALUES (?,?,?,?,?,?,?,?,?,?)";
		params.add(Utils.getFecha("hoy", "dd/MM/yyyy"));
		params.add(distribucion.getCdc());
		params.add(distribucion.getPlaza());
		params.add(distribucion.getRuta());
		params.add(distribucion.getParada());
		params.add(distribucion.getNo_tienda());
		params.add(distribucion.getTda());
		params.add(distribucion.getStatus());
		params.add(this.usuario.getIdUsuario());
		params.add(distribucion.getPaquete());
		
		SQLSelectResult result = exe.executeInsert(SQL, params);
		int affectedRows = result.getAffectedRows();
		
		if (affectedRows == 0) {
			throw new QueryException("No se pudo crear el registro de Carga, favor de verificar. (LOAD0001) TW_Carga Err:"+result.getEx().toString());
			
		}
		else
		{
			distribucion.setIdDistribucion(result.getKey()); 
			estatus = "continue";
		}
		sResult.setEstatus(estatus);
		return sResult;
	}
	public SQLResult compaTiendas()  throws QueryException{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";
		String SQL ="SELECT t1.no_tienda, t1.tda FROM TW_Distribuciones AS t1 "+
					"WHERE NOT EXISTS (SELECT t2.id_tienda FROM TW_Peticiones AS t2 "+
					"WHERE t1.no_tienda = t2.id_tienda AND t2.no_peticion IS NULL) "+
					"AND t1.status = 1";
		SQLSelectResult result = exe.executeSelect(SQL, params);
		if(result.getMessage().equals("continue"))
		{
			List<Hashtable<String,Object>> lista = result.getListResult();
			List<Distribucion> distribuciones = new ArrayList<Distribucion>();
			if(lista.size()>0)
			{
				for(int i=0; i<lista.size();i++)
				{
					Hashtable<String,Object> ht = lista.get(i);
					Distribucion aDistribucion = new Distribucion();
					aDistribucion.setNo_tienda((int)ht.get("no_tienda"));
					aDistribucion.setTda((String)ht.get("tda"));
					distribuciones.add(aDistribucion);
				}
				sResult.setObject(distribuciones);
				estatus = "existente";
			}
			else if(lista.size()==0)
			{
				estatus = "vacio";
			}
		}
		else
		{
			estatus = "error";
			sResult.setEstatus(estatus);
			throw new QueryException("ERR (BOX003): No se puede encontar comparación de distribucion y compración. Err:"+result.getEx().toString());
		}
		sResult.setEstatus(estatus);
		return sResult;
	}
	
	public SQLResult asigDistribucion() throws QueryException{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";
		String SQL ="SELECT idDistribucion, no_tienda FROM TW_Distribuciones "+
				"WHERE status = 1";
		SQLSelectResult result = exe.executeSelect(SQL, params);
		if(result.getMessage().equals("continue"))
		{
			List<Hashtable<String,Object>> lista = result.getListResult();
			if(lista.size()>0)
			{
				for(int i=0; i<lista.size();i++)
				{
					Hashtable<String,Object> ht = lista.get(i);
					ArrayList<Object> param = new ArrayList<Object>();
					String Consult = "UPDATE TW_Peticiones "+
									"SET no_peticion = ? "+
									"WHERE id_tienda = ? AND no_peticion IS NULL";
					param.add(ht.get("idDistribucion"));
					param.add(ht.get("no_tienda"));
					SQLSelectResult resul = exe.executeInsert(Consult, param);
					int affectedRows = resul.getAffectedRows();
					if (affectedRows == 0) {
						throw new QueryException("No se pudo crear la relacion entre las peticiones y las distribuciones. (LOAD0001) TW_Carga Err:"+resul.getEx().toString());
					}
					else
					{
						estatus = "continue";
					}
				}
				ArrayList<Object> par = new ArrayList<Object>();
				String Consul = "UPDATE TW_Distribuciones "+
							"SET status = 0 "+
							"WHERE status = 1";
				SQLSelectResult res = exe.executeInsert(Consul, par);
				int affectRows = res.getAffectedRows();
				if(affectRows == 0){
					throw new QueryException("No se pudo modificar el status de las distribuciones. (LOAD0001) TW_Carga Err:"+res.getEx().toString());
				}else{
					estatus = "continue";
				}
			}
			else if(lista.size()==0)
			{
				estatus = "error";
			}
		}
		else
		{
			estatus = "error";
			sResult.setEstatus(estatus);
			throw new QueryException("ERR (BOX003): No se puede encontar comparación de distribucion y compración. Err:"+result.getEx().toString());
		}
		sResult.setEstatus(estatus);
		return sResult;
	}
	
	public SQLResult buscaDiferencia() throws QueryException{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";
		String SQL ="SELECT id_tienda FROM TW_Peticiones "+
				"WHERE id_tienda = ? AND no_peticion IS NULL";
		params.add(distribucion.getNo_tienda());
		SQLSelectResult result = exe.executeSelect(SQL, params);
		if(result.getMessage().equals("continue"))
		{
			List<Hashtable<String,Object>> lista = result.getListResult();
			if(lista.size()>0)
			{
				estatus = "continue";
			}else if(lista.size()==0){
				estatus = "vacio";
			}
		}
		else
		{
			estatus = "error";
			sResult.setEstatus(estatus);
			throw new QueryException("ERR (BOX003): No se puede encontar comparación de distribucion y compración. Err:"+result.getEx().toString());
		}
		sResult.setEstatus(estatus);
		return sResult;
	}
	public SQLResult paqueteAsig() throws QueryException{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";
		String SQL = "update TW_Distribuciones "+
					 "set paquete = 1 "+
					 "WHERE paquete = 0 AND idEnvio = ?";
		params.add(distribucion.getIdEnvio());
		SQLSelectResult result = exe.executeInsert(SQL, params);
		int affectedRows = result.getAffectedRows();
		if (affectedRows == 0) {
			estatus = "error";
			throw new QueryException("ERR:(BOX004)No se puede modificar el estado del paquete. Err:"+result.getEx().toString());
		}else {
			estatus = "continue";
		}
		sResult.setEstatus(estatus);
		return sResult;
	}
	public SQLResult asigEnvio() throws QueryException{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";
		
		String SQL = "UPDATE TW_Distribuciones "+
					 "SET idEnvio = ? "+
					 "where paquete = 0 AND idEnvio IS NULL";
		params.add(distribucion.getIdEnvio());
		SQLSelectResult result = exe.executeInsert(SQL, params);
		int affectedRows = result.getAffectedRows();
		if (affectedRows == 0) {
			estatus = "error";
			throw new QueryException("ERR:(BOX004)No se pudo modificar el idEnvio. Err:"+result.getEx().toString());
		}
		else
		{
			ArrayList<Object> params1 = new ArrayList<Object>();
			String SQL1 ="SELECT no_tienda, tda, plaza,cdc FROM TW_Distribuciones "+
						"WHERE paquete = 0 AND idEnvio = ?";
			params1.add(distribucion.getIdEnvio());
			SQLSelectResult result1 = exe.executeSelect(SQL1, params1);
			if(result1.getMessage().equals("continue"))
			{
				List<Hashtable<String,Object>> lista1 = result1.getListResult();
				List<Distribucion> distribuciones = new ArrayList<Distribucion>();
				if(lista1.size()>0)
				{
					for(int i=0; i<lista1.size();i++)
					{
						Hashtable<String,Object> ht = lista1.get(i);
						Distribucion aDistribuciones = new Distribucion();
						aDistribuciones.setNo_tienda((int)ht.get("no_tienda"));
						aDistribuciones.setTda((String)ht.get("tda"));
						aDistribuciones.setPlaza((String)ht.get("plaza"));
						aDistribuciones.setCdc((String)ht.get("cdc"));
						distribuciones.add(aDistribuciones);
					}
					sResult.setObject(distribuciones);
					estatus = "continue";
				}else if(lista1.size()==0){
					estatus = "vacio";
				}
			}
			else  
			{
				estatus = "error";
				throw new QueryException("ERR:(ORD0003)ErrorMostrar"+result1.getEx().toString());
			}
		}
		
		sResult.setEstatus(estatus);
		return sResult;
	}
	public SQLResult listaDistr()  throws QueryException{
		SQLResult sResult = new SQLResult();
		ArrayList<Object> params = new ArrayList<Object>();
		String estatus = "continue";
		String SQL ="SELECT no_tienda, tda, plaza,cdc FROM TW_Distribuciones "+
				"WHERE paquete = 0 AND idEnvio = ?";
		params.add(distribucion.getIdEnvio());
		SQLSelectResult result = exe.executeSelect(SQL, params);
		if(result.getMessage().equals("continue"))
		{
			List<Hashtable<String,Object>> lista = result.getListResult();
			List<Distribucion> distribuciones = new ArrayList<Distribucion>();
			if(lista.size()>0)
			{
				for(int i=0; i<lista.size();i++)
				{
					Hashtable<String,Object> ht = lista.get(i);
					Distribucion aDistribuciones = new Distribucion();
					aDistribuciones.setNo_tienda((int)ht.get("no_tienda"));
					aDistribuciones.setTda((String)ht.get("tda"));
					aDistribuciones.setPlaza((String)ht.get("plaza"));
					aDistribuciones.setCdc((String)ht.get("cdc"));
					distribuciones.add(aDistribuciones);
				}
				sResult.setObject(distribuciones);
				estatus = "continue";
			}else if(lista.size()==0){
				estatus = "vacio";
			}
		}else{
			estatus = "error";
			throw new QueryException("ERR:(ORD0003)ErrorMostrar"+result.getEx().toString());
		}
		sResult.setEstatus(estatus);
		return sResult;
		
	}

}
